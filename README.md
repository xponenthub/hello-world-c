# Hello-World-C
Build sample C program with gitlab-ci script.

## Requirement
- Ubuntu 14.04 or 16.04 (tested) or 17.04.

## Download
[![pipeline status](https://gitlab.com/xponenthub/hello-world-c/badges/master/pipeline.svg)](https://gitlab.com/xponenthub/hello-world-c/commits/master)

[Pipelines](/../pipelines) > Artifacts

# Build with Docker

## Setup
1. Install Docker-CE in Ubuntu see https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/.
2. ``$ sudo docker build .``

## How to Run Docker
```
$ docker run hello-world
```

## Load Ubuntu Image
```
$ docker run -it ubuntu
```

## Run bash in Ubuntu Image
```
$ docker run -it ubuntu /bin/bash echo "Hello Ubuntu"
```

**Dockerfile**
```
FROM ubuntu:16.04
RUN apt-get update && apt-get install -y \
&& echo "Hello Ubuntu"
```

## References
