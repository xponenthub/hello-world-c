FROM gcc:4.9
COPY . ~/Desktop/hello-world-c
WORKDIR ~/Desktop/hello-world-c
RUN gcc main.c -o main
CMD ["./main"]
